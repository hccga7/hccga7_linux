#   python /home/hcc/bitbucket/hccga7_linux/RL_Proj001/Chap02_002.py

#注意：需另行安裝box2d 否則執行 env = gym.make('CarRacing-v0') 時會報錯： No module named 'Box2D'
#安裝指令如下
#(universe) hcc@hcc-pc:~$ pip install box2d-py

#注意：在Python 3.7 的 pyglet 會報錯
'''
Track generation: 1240..1554 -> 314-tiles track
Traceback (most recent call last):
  File "/usr/lib/python3.7/site-packages/pyglet/text/runlist.py", line 408, in ranges
    starts[i], ends[i], values[i] = next(iterator)
StopIteration

The above exception was the direct cause of the following exception:

Traceback (most recent call last):
  File "/home/hcc/.vscode/extensions/ms-python.python-2019.6.22090/pythonFiles/ptvsd_launcher.py", line 43, in <module>
    main(ptvsdArgs)
  File "/home/hcc/.vscode/extensions/ms-python.python-2019.6.22090/pythonFiles/lib/python/ptvsd/__main__.py", line 434, in main
    run()
  File "/home/hcc/.vscode/extensions/ms-python.python-2019.6.22090/pythonFiles/lib/python/ptvsd/__main__.py", line 312, in run_file
    runpy.run_path(target, run_name='__main__')
  File "/usr/lib64/python3.7/runpy.py", line 263, in run_path
    pkg_name=pkg_name, script_name=fname)
  File "/usr/lib64/python3.7/runpy.py", line 96, in _run_module_code
    mod_name, mod_spec, pkg_name, script_name)
  File "/usr/lib64/python3.7/runpy.py", line 85, in _run_code
    exec(code, run_globals)
  File "/home/hcc/Bitbucket/hccga7_linux/RL_Proj001/Chap02_002.py", line 14, in <module>
    env.reset()
  File "/usr/lib/python3.7/site-packages/gym/wrappers/time_limit.py", line 24, in reset
    return self.env.reset(**kwargs)
  File "/usr/lib/python3.7/site-packages/gym/envs/box2d/car_racing.py", line 311, in reset
    return self.step(None)[0]
  File "/usr/lib/python3.7/site-packages/gym/envs/box2d/car_racing.py", line 323, in step
    self.state = self.render("state_pixels")
  File "/usr/lib/python3.7/site-packages/gym/envs/box2d/car_racing.py", line 350, in render
    color=(255,255,255,255))
  File "/usr/lib/python3.7/site-packages/pyglet/text/__init__.py", line 453, in __init__
    multiline, dpi, batch, group)
  File "/usr/lib/python3.7/site-packages/pyglet/text/__init__.py", line 275, in __init__
    dpi=dpi, batch=batch, group=group)
  File "/usr/lib/python3.7/site-packages/pyglet/text/layout.py", line 810, in __init__
    self.document = document
  File "/usr/lib/python3.7/site-packages/pyglet/text/layout.py", line 919, in _set_document
    self._init_document()
  File "/usr/lib/python3.7/site-packages/pyglet/text/layout.py", line 1020, in _init_document
    self._update()
  File "/usr/lib/python3.7/site-packages/pyglet/text/layout.py", line 956, in _update
    lines = self._get_lines()
  File "/usr/lib/python3.7/site-packages/pyglet/text/layout.py", line 932, in _get_lines
    glyphs = self._get_glyphs()
  File "/usr/lib/python3.7/site-packages/pyglet/text/layout.py", line 1055, in _get_glyphs
    for start, end, (font, element) in runs.ranges(0, len(text)):
RuntimeError: generator raised StopIteration
'''

import sys
sys.path.append('/home/hcc/anaconda3/envs/universe/lib/python3.6/site-packages')
import time
import gym
env = gym.make('CarRacing-v0')
env.reset()
for _ in range(10000):
	env.render()
	obs, rew, done, info = env.step(env.action_space.sample())
	#time.sleep(0.02)
	if done:
		env.reset()
env.close()
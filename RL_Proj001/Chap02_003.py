import gym
env = gym.make('BipedalWalker-v2')
for i_epsode in range(100):
	observation = env.reset()
	for t in range(500):
		env.render()
		print(observation)
		action = env.action_space.sample()
		observation, reward, done, info = env.step(action)
		if done:
			env.reset()
			print("{} timesteps taken for the episode".format(t+1))
env.close()
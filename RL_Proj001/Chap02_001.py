#   python /home/hcc/bitbucket/hccga7_linux/RL_Proj001/Chap02_001.py

import sys
sys.path.append('/home/hcc/anaconda3/envs/universe/lib/python3.6/site-packages')
import time
import gym
env = gym.make('CartPole-v0')
env.reset()
for _ in range(10000):
	env.render()
	obs, rew, done, info = env.step(env.action_space.sample())
	time.sleep(0.02)
	if done:
		env.reset()
env.close()
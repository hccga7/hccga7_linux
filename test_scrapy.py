# -*- coding: utf-8 -*
#   python /home/hcc/bitbucket/hccga7_linux/test_scrapy.py
#   python /home/hcc/bitbucket/hccga7_linux/test_scrapy.py '5371' '2018' '4' 'Y'
# import sys
# sys.path.insert(0, "/home/hcc/bitbucket/hccga7_linux/src/hcc/scrapy/stock/")

# test001： 公用下載函式
'''
url = 'http://mops.twse.com.tw/mops/web/t164sb04'
payload = {
  'encodeURIComponent': '1', 'step': '1', 'firstin': '1', 'off': '1', 'keyword4': '', 'code1': '', 'TYPEK2': '', 'checkbtn': '', 'queryName': 'co_id', 'inpuType': 'co_id'
  , 'TYPEK': 'all', 'isnew': 'false'
  , 'co_id': '2330'
  , 'year': '108'
  , 'season': '04'
	}
encoding = 'utf8'

from src.hcc.scrapy.stock import mops
response_html = mops.download(url,payload,encoding)
print(response_html)
'''

'''
def getChangeRate(this_year_total,last_year_total):
	import sys
	from decimal import Decimal
	result_ChangeRate = ''
	try:
		this_year_total = this_year_total.strip().replace(",", "")
		last_year_total = last_year_total.strip().replace(",", "")
		decimal_this_year_total = Decimal(this_year_total)
		decimal_last_year_total = Decimal(last_year_total)
		change_percentage = (decimal_this_year_total - decimal_last_year_total)/decimal_last_year_total * 100
		change_percentage = round(change_percentage,2)
		result_ChangeRate = str(change_percentage) + '%'
	except:
		print("---------[getChangeRate() error]:Unexpected error:", sys.exc_info()[0])
		result_ChangeRate = ''
	return result_ChangeRate
'''

# test002： 個別綜合損益表
'''
try:
	1/0
except Exception as e:
	print(e)
	raise
else:
	pass
finally:
	pass

from src.hcc.scrapy.stock import mops
response_html = mops.download_t164sb04('2474',2018,4)
#print(response_html)
from lxml import html
tree = html.fromstring(response_html)
trs = tree.cssselect('table.hasBorder tr')
idx_tr = 0
for tr in trs:
	idx_tr = idx_tr + 1
	tds = tr.getchildren()
	if idx_tr == 1:
		print(tds[0].text.strip())
	if tds[0].text.strip() == u'本期綜合損益總額':
		print(tds[0].text.strip() + ': ' + tds[1].text.strip() + u'\t去年度: ' + tds[3].text.strip())
		print(getChangeRate(tds[1].text, tds[3].text))
	if tds[0].text.strip() == u'基本每股盈餘' and not tds[1].text is None:
		print(tds[0].text.strip() + ': ' + tds[1].text.strip())
'''
from src.hcc.parse.stock import mops
import src.hcc.scrapy.stock


def repeat_t164sb04():
    filename_t164sb04 = '/home/hcc/' + co_id + '_' + str(year) + '_Q' + str(season) + '_t164sb04.html'
    if save_file:
        m1.download_t164sb04(co_id, year, season)
        m1.save_file_t164sb04(co_id, year, season, filename_t164sb04)
        print('download and save OK: ' + filename_t164sb04)
    else:
        m1.set_response_html_t164sb04_from_file(filename_t164sb04)
        print('read OK: ' + filename_t164sb04)

    d1 = m1.get_t164sb04_net_income_this_year_season(co_id, year, season)
    print(d1)
    d1 = m1.get_t164sb04_net_income_last_year_season(co_id, year, season)
    print(d1)
    d1 = m1.get_t164sb04_eps_this_year_season(co_id, year, season)
    print(d1)


# m1.save_file_t164sb04('2105', 2018, 4, '2105_2018_Q4_t164sb04.html')

# s1 = m1.get_string_from_file('2105_2018_Q4_t164sb04.html')
# print(s1)
import sys
from distutils.util import strtobool
co_id = sys.argv[1]
year = int(sys.argv[2])
season = int(sys.argv[3])
save_file = strtobool(sys.argv[4])



py_exe1 = '/home/hcc/bitbucket/hccga7_linux/Fin_Analysis_Proj001/_Fin_Final_python/Mops_t05st10_ifrs.py'
py_exe2 = '/home/hcc/bitbucket/hccga7_linux/Fin_Analysis_Proj001/_Fin_Final_python/Mops_t05st25_q1.py'
py_exe4 = '/home/hcc/bitbucket/hccga7_linux/Fin_Analysis_Proj001/20190624_python/cmoney.py'

# 營收
sys.argv = [py_exe1, co_id]
#exec(compile(open(py_exe1, "rb").read(), py_exe1, 'exec'))

# region Description [custom code folding pycharm]
'''
https://www.jetbrains.com/help/objc/code-folding.html#supported_comments

Surrounding a fragment with folding comments #
Select the code fragment of interest.

Select Code | Surround With or press⌥⌘T.

Select the folding comments to be used.

Specify the fragment description.
Now if you collapse the fragment, the description you have specified is shown in place of the code.

https://stackoverflow.com/questions/25135328/how-do-i-make-pycharm-stop-hiding-unfold-my-python-imports/25156495
Open the IDE Settings (File > Settings, or Ctrl+Alt+S).
Under the "Editor" node, click "General" and then "Code Folding". The "Code Folding" page is displayed.
In the "Collapse by default list", select the check boxes to the left of the code constructs you want to be displayed collapsed. So here you can uncheck "Imports".
Apply changes.
'''
# 獲利
m1 = mops.MopsParser()
year = 2019
season = 1
#repeat_t164sb04()

m1 = mops.MopsParser()
year = 2018
season = 4
#repeat_t164sb04()

m1 = mops.MopsParser()
year = 2018
season = 3
#repeat_t164sb04()

m1 = mops.MopsParser()
year = 2018
season = 2
#repeat_t164sb04()

m1 = mops.MopsParser()
year = 2018
season = 1
#repeat_t164sb04()
# endregion

'''
co_id = '2493'
year = 2018
season = 4
save_file = True
'''

# 週轉
sys.argv = [py_exe2, co_id]
#exec(compile(open(py_exe2, "rb").read(), py_exe2, 'exec')))

# CashFlow

from src.hcc.scrapy.stock import mops
response_json = mops.download_cmoney_freecf_json(co_id)
m1.save_file(response_json.encode(), co_id + '_fcf.json')

m1.parse_cmoney_freecf(co_id)
print(str(m1.year_cmoney_freecf01) + 'Q' + str(m1.season_cmoney_freecf01) + ': ' + str(m1.decimal_cmoney_freecf01))
print('')
print(str(m1.year_cmoney_freecf02) + 'Q' + str(m1.season_cmoney_freecf02) + ': ' + str(m1.decimal_cmoney_freecf02))
print('')
print(str(m1.year_cmoney_freecf03) + 'Q' + str(m1.season_cmoney_freecf03) + ': ' + str(m1.decimal_cmoney_freecf03))
print('')
print(str(m1.year_cmoney_freecf04) + 'Q' + str(m1.season_cmoney_freecf04) + ': ' + str(m1.decimal_cmoney_freecf04))
print('')
print(str(m1.year_cmoney_freecf05) + 'Q' + str(m1.season_cmoney_freecf05) + ': ' + str(m1.decimal_cmoney_freecf05))
print('')
print(str(m1.year_cmoney_freecf06) + 'Q' + str(m1.season_cmoney_freecf06) + ': ' + str(m1.decimal_cmoney_freecf06))
print('')
print(str(m1.year_cmoney_freecf07) + 'Q' + str(m1.season_cmoney_freecf07) + ': ' + str(m1.decimal_cmoney_freecf07))
print('')
print(str(m1.year_cmoney_freecf08) + 'Q' + str(m1.season_cmoney_freecf08) + ': ' + str(m1.decimal_cmoney_freecf08))



import pymssql
from time import gmtime, strftime
import time
strdt = strftime("%Y-%m-%d %H:%M:%S", time.localtime())
#strdt = strftime("%Y-%m-%d %H:%M:%S", gmtime())


conn = pymssql.connect('ip', 'user', 'password', 'db')

cursor = conn.cursor()

cursor.executemany(
    "INSERT INTO tRAW_BD_INDEX05 VALUES (%s, %d, %d, %d, %s)",
    [(co_id, m1.year_cmoney_freecf01, m1.season_cmoney_freecf01, m1.decimal_cmoney_freecf01, strdt),
     (co_id, m1.year_cmoney_freecf02, m1.season_cmoney_freecf02, m1.decimal_cmoney_freecf02, strdt),
     (co_id, m1.year_cmoney_freecf03, m1.season_cmoney_freecf03, m1.decimal_cmoney_freecf03, strdt),
     (co_id, m1.year_cmoney_freecf04, m1.season_cmoney_freecf04, m1.decimal_cmoney_freecf04, strdt),
     (co_id, m1.year_cmoney_freecf05, m1.season_cmoney_freecf05, m1.decimal_cmoney_freecf05, strdt),
     (co_id, m1.year_cmoney_freecf06, m1.season_cmoney_freecf06, m1.decimal_cmoney_freecf06, strdt),
     (co_id, m1.year_cmoney_freecf07, m1.season_cmoney_freecf07, m1.decimal_cmoney_freecf07, strdt),
     (co_id, m1.year_cmoney_freecf08, m1.season_cmoney_freecf08, m1.decimal_cmoney_freecf08, strdt)])
'''
cursor.execute("""
	INSERT INTO tRAW_BD_INDEX05 VALUES ('1101', m1.year_cmoney_freecf01, m1.season_cmoney_freecf01, m1.decimal_cmoney_freecf01, GETDATE())
""")
'''
conn.commit()
'''
cursor.execute('SELECT TOP 5 * FROM tTDCCTemp')
row = cursor.fetchone()
while row:
  print("Code=%s" % (row[0]))
  row = cursor.fetchone()
 '''
# -*- coding: utf-8 -*
#   python /home/hcc/Bitbucket/hccga7_linux/testComey.py '8499'
#   python /home/hcc/Bitbucket/hccga7_linux/testComey.py 8499

from src.hcc.parse.stock import cmoney as ps
import json
log_msg = []

def DoLog(msg):
    log_msg.append(msg)
    print(msg)

def save_json(filename, json):
    file = open(filename, 'w')
    file.write(json)
    file.close()

def get_json(co_id, to_save):
    c = ps.CmoneyParser()
    json29 = c.getJson(co_id,'29')
    #json41 = c.getJson(co_id,'41')
    json42 = c.getJson(co_id,'42')
    json43 = c.getJson(co_id,'43')
    GetFromJson43_OperatingProfitMargin(json43)
    DoLog('----------------------------------------------')
    GetFromJson29_MonthlyRevenueYearGrowth(json29, 6)
    DoLog('----------------------------------------------')
    GetFromJson43_EPS(json43)
    DoLog('----------------------------------------------')
    GetFromJson43_NetProfitGrowthRatio(json43)
    DoLog('----------------------------------------------')
    GetFromJson42_FreeCashFlow(json42)
    DoLog('----------------------------------------------')
    GetFromJson43_InventoryTurnoverRatio(json43)
    if to_save:
        save_json(co_id + '_29.json', json29)
        #save_json(co_id + '_41.json', json41)
        save_json(co_id + '_42.json', json42)
        save_json(co_id + '_43.json', json43)

def read_json_from_file(json_file_name):
    with open(json_file_name, 'r') as file:
        data = file.read().replace('\n', '')
        return data

def GetEpsFromFile43(json_file_name):
    data = read_json_from_file(json_file_name)
    GetFromJson43_EPS(data)

def GetFromJson43_EPS(data):
    j = json.loads(data)
    for r in j:	
        try:
            amt = round(float(r['EPS']),2)
        except:
            amt =  float('nan')
        DoLog(r['DateRange'] + ' EPS : ' + str(amt))

def GetFromJson43_OperatingProfitMargin(data):
    j = json.loads(data)
    for r in j:
        try:
            amt = round(float(r['OperatingProfitMargin']),2)
        except:
            amt =  float('nan')
        DoLog(r['DateRange'] + ' 營業利益率 : ' + str(amt))

def GetFromJson29_MonthlyRevenueYearGrowth(data, datacnt):
    j = json.loads(data)
    idx = 0
    for r in j:	
        idx = idx + 1
        if idx > datacnt:
            return
        try:
            amt = round(float(r['MonthlyRevenueYearGrowth']),2)
        except:
            amt =  float('nan')
        DoLog(r['Date'] + ' 單月年增率 : ' + str(amt))

def GetFromJson43_NetProfitGrowthRatio(data):
    j = json.loads(data)
    for r in j:	
        try:
            amt = round(float(r['NetProfitGrowthRatio']),2)
        except:
            amt =  float('nan')
        DoLog(r['DateRange'] + ' 稅後純益成長率 : ' + str(amt))

def GetFromJson43_InventoryTurnoverRatio(data):
    j = json.loads(data)
    for r in j:	
        try:
            amt = round(float(r['InventoryTurnoverRatio']),2)
        except:
            amt =  float('nan')
        DoLog(r['DateRange'] + ' 存貨週轉率 : ' + str(amt))

def GetFromJson42_FreeCashFlow(data):
    j = json.loads(data)
    for r in j:	
        try:
            amt = round(float(r['FreeCashFlow'])/1000,2)
        except Exception as e:   #https://dotblogs.com.tw/caubekimo/2018/09/17/145733   [Python] 當Exception發生時，怎麼抓它發生的位置以及詳細原因？
            DoLog(e.args[0])
            amt =  float('nan')
        DoLog(r['DateRange'] + ' 自由現金流量(百萬) : ' + str(amt))

#GetEpsFromFile43('8499_43.json')
#get_json('4535', True)

'''
data29 = read_json_from_file('8499_29.json')
data42 = read_json_from_file('8499_42.json')
data43 = read_json_from_file('8499_43.json')

GetFromJson43_OperatingProfitMargin(data43)
GetFromJson29_MonthlyRevenueYearGrowth(data29, 6)
GetFromJson43_EPS(data43)
GetFromJson43_NetProfitGrowthRatio(data43)
GetFromJson42_FreeCashFlow(data42)
GetFromJson43_InventoryTurnoverRatio(data43)
'''


#迴圈版---------------------------------------------------------------------------
import time
sleep_seconds = 30
with open('/home/hcc/tmp/co_id_list.txt') as f:
    co_id_list = f.read().splitlines()
    for co_id in co_id_list:
        log_msg = []
        get_json(co_id, False)
        with open('/home/hcc/tmp/cmoney_json/QueryCmLog_' + co_id + '.txt', 'w') as f:
            for item in log_msg:
                f.write("%s\n" % item)
        time.sleep(sleep_seconds)





#單一版---------------------------------------------------------------------------
'''
import sys
co_id = sys.argv[1]
#get_json(co_id, True)
get_json(co_id, False)
#Writing a list to a file with Python
#https://stackoverflow.com/questions/899103/writing-a-list-to-a-file-with-python
with open('QueryCmLog_' + co_id + '.txt', 'w') as f:
    for item in log_msg:
        f.write("%s\n" % item)
'''
#單一版---------------------------------------------------------------------------
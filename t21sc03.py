import pandas as pd
import requests
from io import StringIO
import time
import codecs

def get_url(co_market, co_kind, year, month):
    # 假如是西元，轉成民國
    if year > 1990:
        year -= 1911

    url = 'https://mops.twse.com.tw/nas/t21/' + co_market + '/t21sc03_'+str(year)+'_'+str(month)+'_' + co_kind +'.html'
    if year <= 98:
        url = 'https://mops.twse.com.tw/nas/t21/' + co_market + '/t21sc03_'+str(year)+'_'+str(month)+'.html'

    return url

def monthly_report_common(url):    
    # 偽瀏覽器
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    
    # 下載該年月的網站，並用pandas轉換成 dataframe
    r = requests.get(url, headers=headers)
    r.encoding = 'big5'

    dfs = pd.read_html(StringIO(r.text), encoding='big-5')

    df = pd.concat([df for df in dfs if df.shape[1] <= 11 and df.shape[1] > 5])
    
    if 'levels' in dir(df.columns):
        df.columns = df.columns.get_level_values(1)
    else:
        df = df[list(range(0,10))]
        column_index = df.index[(df[0] == '公司代號')][0]
        df.columns = df.iloc[column_index]
    
    df['當月營收'] = pd.to_numeric(df['當月營收'], 'coerce')
    df = df[~df['當月營收'].isnull()]
    df = df[df['公司代號'] != '合計']
    
    # 偽停頓
    time.sleep(5)

    return df

'''
超簡單用python抓取每月營收
https://www.finlab.tw/%E8%B6%85%E7%B0%A1%E5%96%AE%E7%94%A8python%E6%8A%93%E5%8F%96%E6%AF%8F%E6%9C%88%E7%87%9F%E6%94%B6/
https://mops.twse.com.tw/nas/t21/sii/t21sc03_108_6_0.html	國內上市
https://mops.twse.com.tw/nas/t21/sii/t21sc03_108_6_1.html	國外上市
https://mops.twse.com.tw/nas/t21/otc/t21sc03_108_6_0.html	國內上櫃
https://mops.twse.com.tw/nas/t21/otc/t21sc03_108_6_1.html	國外上櫃
'''

path_folder = '/home/hcc/tmp/'
path_folder_windows = 'R:\\201911\\'

year = 2019
month = 11
seconds = 30

type_index = 2
filter_growth_rate = 0

import platform
if platform.system() == 'Windows':
    path_folder = path_folder_windows

from decimal import Decimal


'''本區段執行下載==============================================================

data = monthly_report_common(get_url('sii', '0', year, month))
sii_0 = data.sort_values('去年同月增減(%)', ascending=False)
sii_0.insert(type_index, "公司種類", "sii_0")
sii_0.to_csv(path_folder + 'sii_0.csv')

data = monthly_report_common(get_url('sii', '1', year, month))
sii_1 = data.sort_values('去年同月增減(%)', ascending=False)
sii_1.insert(type_index, "公司種類", "sii_1")
sii_1.to_csv(path_folder + 'sii_1.csv')

data = monthly_report_common(get_url('otc', '0', year, month))
otc_0 = data.sort_values('去年同月增減(%)', ascending=False)
otc_0.insert(type_index, "公司種類", "otc_0")
otc_0.to_csv(path_folder + 'otc_0.csv')

data = monthly_report_common(get_url('otc', '1', year, month))
otc_1 = data.sort_values('去年同月增減(%)', ascending=False)
otc_1.insert(type_index, "公司種類", "otc_1")
otc_1.to_csv(path_folder + 'otc_1.csv')

all_stock = pd.concat([sii_0,sii_1,otc_0,otc_1],axis=0)
all_stock = all_stock.sort_values('去年同月增減(%)', ascending=False)
all_stock.to_csv(path_folder + 'all_stock.csv')

'''





def ParseJson(data, datacnt,json_key1,json_key2,title,denominator):
    ret = {}
    j = json.loads(data)    
    idx = 0
    print(title)
    for r in j:	
        idx = idx + 1
        if idx > datacnt:
            return ret
        try:
            ret[r[json_key1]] = round(float(r[json_key2])/denominator,2)
            print(ret[r[json_key1]])
        except:
            ret[r[json_key1]] =  float('nan')
            print(ret[r[json_key1]])



def ParseDict(dict_name, dict_data, splitter):
    ret = ''
    if dict_data is None:
        return ret
    #print(dict_name)
    for k, v in dict_data.items():
        ret = ret + dict_name + splitter + k + splitter + str(v) + splitter
        #print(k)
        #print(v)
    return ret








import json
from src.hcc.scrapy.stock import cmoney
khtml = cmoney.getAllCmkeyHtml('2330')


cmkind = ['29','41','42','43']
final_data_array = []

df = pd.read_csv(path_folder + 'all_stock.csv')
df = df[df["去年同月增減(%)"] >= filter_growth_rate][["公司代號","公司名稱","公司種類","去年同月增減(%)"]]
for index, row in df.iterrows():
    final_data = ''
    cmoney_jsonArray = []
    print(row['公司代號'], row['公司名稱'], row['公司種類'], row['去年同月增減(%)'])
    for i in range(0,len(cmkind)):
        json_i = cmoney.GetJson(row['公司代號'],cmkind[i],khtml)
        cmoney_jsonArray.append( json_i )
        #with open(path_folder + 'output\\' + row['公司代號'] + '_' + str(i) + '.json', 'w') as filehandle:
            #json.dump(json_i, filehandle)
    json1 = ParseJson(cmoney_jsonArray[3], 4,'DateRange', 'OperatingProfitMargin', u'營業利益率',1)
    json2 = ParseJson(cmoney_jsonArray[0], 6,'Date', 'MonthlyRevenueYearGrowth', u'營收單月年增率',1)
    json3 = ParseJson(cmoney_jsonArray[3], 4,'DateRange', 'EPS', u'EPS',1)
    json4 = ParseJson(cmoney_jsonArray[3], 4,'DateRange', 'NetProfitGrowthRatio', u'稅後純益成長率',1)
    json5 = ParseJson(cmoney_jsonArray[2], 4,'DateRange', 'FreeCashFlow', u'自由現金流量(百萬)',1000)
    json6 = ParseJson(cmoney_jsonArray[3], 4,'DateRange', 'InventoryTurnoverRatio', u'存貨週轉率',1)
    
    final_data = row['公司代號'] + ','
    final_data = final_data + ParseDict(u'營業利益率', json1, ',')
    final_data = final_data + ParseDict(u'營收單月年增率', json2, ',')
    final_data = final_data + ParseDict(u'EPS', json3, ',')
    final_data = final_data + ParseDict(u'稅後純益成長率', json4, ',')
    final_data = final_data + ParseDict(u'自由現金流量(百萬)', json5, ',')
    final_data = final_data + ParseDict(u'存貨週轉率', json6, ',')
    with codecs.open(path_folder + 'output\\' + row['公司代號'] + '_final_data.csv', 'w', 'utf-8-sig') as filehandle:
        #json.dump(final_data, filehandle)
        filehandle.write(final_data)

    final_data_array.append(final_data)    
    with codecs.open(path_folder + 'output\\' + 'final_data_array.csv', 'w', 'utf-8-sig') as outfile:
        outfile.write("\n".join(final_data_array))
	
    #with open(path_folder + 'output\\' + row['公司代號'] + '.json', 'w') as filehandle:
    #    json.dump(cmoney_jsonArray, filehandle)
    time.sleep( seconds )
#df["公司代號"] == "2514"
#df["去年同月增減(%)"] >= 10
#df[df["去年同月增減(%)"] >= 10]

'''
idx = 0
for index, row in X.iterrows():
    idx = idx+1
    try:
        g_rate = Decimal(row['去年同月增減(%)'])
        if(g_rate>10):
            print(row['公司名稱'], row['公司代號'], g_rate, idx)
    except Exception as e:
        #print(e)
        print(g_rate)
'''
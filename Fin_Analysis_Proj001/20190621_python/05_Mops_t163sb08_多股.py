# -*- coding: utf-8 -*
#有中文字時會出錯   Non-ASCII character '\xe7'   ，因此檔案第一行要加coding: utf-8

#python Mops_t163sb08.py
class Mops_t163sb08:
 def __init__(self, co_id, year):
  self.__url = 'http://mops.twse.com.tw/mops/web/t163sb08'
  self.__encoding = 'utf8'
  self.__cssselect = 'table.hasBorder tr'
  self.co_id = co_id
  self.year_tw = year - 1911
  self.list_info = []
  
 def parse(self):
  from decimal import Decimal
  import requests
  from lxml import etree
  payload = {'encodeURIComponent': '1', 'step': '1', 'firstin': '1', 'off': '1', 'keyword4': '', 'code1': '', 'TYPEK2': '', 'checkbtn': '', 'queryName': 'co_id', 't05st29_c_ifrs': 'N', 't05st30_c_ifrs': 'N', 'inpuType': 'co_id', 'TYPEK': 'all', 'isnew': 'false', 'co_id': self.co_id, 'year': str(self.year_tw)}
  r = requests.post(self.__url, data = payload)
  r.encoding = self.__encoding
  from lxml import html
  tree = html.fromstring(r.content)
  trs = tree.cssselect(self.__cssselect)
  idx_tr = 0
  tr_year = 1911
  idx_quarter = 0
  total_quarters = 0
  is_new_year_tr = False
  tr_info = ""
  for tr in trs:
    idx_tr = idx_tr + 1
    if idx_tr == 1:
      continue
    if(len(tr_info)>0):
      self.list_info.append(tr_info)
    tds = tr.getchildren()
    for td in tds:
    #print td.text
    #print td.tag
      is_new_year_tr = type(td.get("rowspan")) is str
      if is_new_year_tr:
        total_quarters = int(td.get("rowspan"))
        tr_year = int(td.text.replace(u"年度", "")) + 1911
        self.list_info.append(",,,,,,")
      elif td.tag == 'th':
        if td.text == u'第一季':
          idx_quarter = 1
        elif td.text == u'半年度':
          idx_quarter = 2
        elif td.text == u'第三季':
          idx_quarter = 3
        elif td.text == u'全年度':
          idx_quarter = 4
        else:
          idx_quarter = 0
          print('idx_quarter rror-----------------------------------------')
        #print tr_info
        tr_info = str(tr_year) + ',' + str(idx_quarter)
      else:
        #print Decimal(td.text.replace(",", ""))
        tr_info = tr_info + ',' + td.text.replace(",", "")

  self.list_info.append(tr_info)
  return self.list_info






idx_num = 0
num_of_files_to_sleep = 3
seconds_to_sleep = 30

import time
with open('list.txt') as f:
    lines = f.read().splitlines()
for li in lines:
  idx_num = idx_num + 1
  if idx_num % num_of_files_to_sleep == 0:
    print "---------sleep : %s" % time.ctime()
    time.sleep(seconds_to_sleep)
    #print "End : %s" % time.ctime()
  co_id = li
  co1 = Mops_t163sb08(co_id,2013)
  co2 = Mops_t163sb08(co_id,2016)
  co3 = Mops_t163sb08(co_id,2019)
  my_list = co1.parse() + co2.parse() + co3.parse()
  with open(co_id + '.csv', 'w') as f:
      for item in my_list:
          f.write("%s\n" % item)
  print(co_id + ' OK')

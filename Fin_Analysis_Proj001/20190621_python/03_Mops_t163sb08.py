#python Mops_t163sb08.py
class Mops_t163sb08:
 def __init__(self, co_id, year):
  self.__url = 'http://mops.twse.com.tw/mops/web/t163sb08'
  self.__encoding = 'utf8'
  self.__cssselect = 'table.hasBorder tr'
  self.co_id = co_id
  self.year_tw = year - 1911
  
 def parse(self):
  import requests
  from lxml import etree
  payload = {'encodeURIComponent': '1', 'step': '1', 'firstin': '1', 'off': '1', 'keyword4': '', 'code1': '', 'TYPEK2': '', 'checkbtn': '', 'queryName': 'co_id', 't05st29_c_ifrs': 'N', 't05st30_c_ifrs': 'N', 'inpuType': 'co_id', 'TYPEK': 'all', 'isnew': 'false', 'co_id': self.co_id, 'year': str(self.year_tw)}
  r = requests.post(self.__url, data = payload)
  r.encoding = self.__encoding
  from lxml import html
  tree = html.fromstring(r.content)
  trs = tree.cssselect(self.__cssselect)
  for tr in trs:
   tds = tr.getchildren()
   for td in tds:
    print td.text
    print td.tag
    #trs[1].getchildren()[0].get("rowspan")
    
co1 = Mops_t163sb08('2330',2019)
co1.parse()
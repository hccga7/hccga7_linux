# -*- coding: utf-8 -*
'''
encodeURIComponent: 1
step: 1
firstin: 1
off: 1
keyword4: 
code1: 
TYPEK2: 
checkbtn: 
queryName: co_id
inpuType: co_id
TYPEK: all
isnew: false
co_id: 2330
year: 108
month: 02
'''

#   python Mops_t05st10_ifrs.py '2330'

revenue_this_year = ''
revenue_last_year = ''
change_per100 = ''

url = 'http://mops.twse.com.tw/mops/web/t05st10_ifrs'
encoding = 'utf8'
result_string = ''

def parse(co_id,year,month):
	year_tw = str(year-1911)
	if month < 10:
		month_str = '0' + str(month)
	else:
		month_str = str(month)
	import requests
	from lxml import etree
	payload = {
	  'encodeURIComponent': '1', 'step': '1', 'firstin': '1', 'off': '1', 'keyword4': ''  , 'code1': '', 'TYPEK2': '', 'checkbtn': '', 'queryName': 'co_id'  , 'inpuType': 'co_id', 'TYPEK': 'all', 'isnew': 'false'
	  , 'co_id': co_id
	  , 'year': year_tw
	  , 'month': month_str
	}
	r = requests.post(url, data = payload)
	r.encoding = encoding
	from lxml import html
	tree = html.fromstring(r.content)
	trs = tree.cssselect('table.hasBorder tr')
	for tr in trs:
		tds = tr.getchildren()
		if tds[0].text == u'本月':
			revenue_this_year = tds[1].text.replace(",", "").strip()
			#print revenue_this_year
		elif tds[0].text == u'去年同期':
			revenue_last_year = tds[1].text.replace(",", "").strip()
			#print revenue_last_year
		elif tds[0].text == u'增減百分比':
			change_per100 = tds[1].text.replace(",", "").strip()
			#print change_per100
			break
	
	result_string = co_id + ',' + str(year) + ',' + str(month) + ',' + revenue_this_year + ',' + revenue_last_year + ',' + change_per100
	
	return result_string


'''
print(parse('2330',2018,11))
print(parse('2330',2018,12))
print(parse('2330',2018,10))
print(parse(py_co_id,py_year,py_month))
'''

import sys
import datetime
import time
py_co_id = sys.argv[1]
seconds_to_sleep = 3


print(parse(py_co_id,2019,5))
print(parse(py_co_id,2019,4))
print(parse(py_co_id,2019,3))
print(parse(py_co_id,2019,2))
print(parse(py_co_id,2019,1))
print(parse(py_co_id,2018,12))
'''
for py_year in range(2013,datetime.datetime.now().year+1):
	for py_month in range(1,13):
		if py_year < datetime.datetime.now().year:
			#print str(py_year) + '_' + str(py_month)
			print(parse(py_co_id,py_year,py_month))
		else:
			if py_month <= datetime.datetime.now().month:
				#print str(py_year) + '_' + str(py_month)
				print(parse(py_co_id,py_year,py_month))
		time.sleep(seconds_to_sleep)
'''
#print(parse(py_co_id,2018,11))
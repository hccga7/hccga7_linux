# -*- coding: utf-8 -*
# python Pandas_t163sb08.py '2330'

#開檔
import sys
py_co_id = sys.argv[1]
py_result = 0.0

import pandas as pd
df = pd.read_csv(py_co_id + '.csv')
d = pd.DataFrame(df)
#print(d)
#print(d.loc[2])

d = d.dropna()
#print(d)

#df.columns = ['year', 'qtr', 'revenue', u'毛利率', u'營業利益率', u'稅前純益率', u'稅後純益率']

new_columns = df.columns.values
new_columns[0] = 'year'
new_columns[1] = 'qtr'
new_columns[2] = 'revenue'
new_columns[3] = u'毛利率'
new_columns[4] = u'營業利益率'
new_columns[5] = u'稅前純益率'
new_columns[6] = u'稅後純益率'
df.columns = new_columns

#print(d.describe())
py_result = df[u'營業利益率'].mean()
print(py_co_id + u'的估計營業利益率為： ' + str(py_result))

hs = open("hst.txt","a")
hs.write(py_co_id + u',' + str(py_result) + '\n')
hs.close() 
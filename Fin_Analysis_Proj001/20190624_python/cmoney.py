# -*- coding: utf-8 -*
#   python /home/hcc/bitbucket/hccga7_linux/Fin_Analysis_Proj001/20190624_python/cmoney.py '2701' 'Y'
#   http://www.cmoney.tw/finance/f00042.aspx?s=9940

#import urllib.parse
#urllib.parse.urlencode('CF6a1IO8dx6Caduocze85A==')

#https://jsonformatter.curiousconcept.com/

import sys
from distutils.util import strtobool
co_id = sys.argv[1]
to_save_json = strtobool(sys.argv[2])
encoding = 'utf8'
cookies = dict(_ga='GA1.2.1567679139.1525861239'
	, __gads='ID=be7760519b14e770:T=1525861238:S=ALNI_MYSebuXGIFWVB7SdznkD1Cev3tLlA'
	, __auc = '9d320dad16b75724ae281d02957'
	, _fbp = 'fb.1.1561043554425.739071605'
	, AspSession = 'lugpcwv0co24acghvhj3wden'
	, NET_SessionId = 'knfvra2gizicwrk3rnyysf1h'
	, _gid = 'GA1.2.370897868.1561398716'
	, _gat_real = '1'
	)
#print(cookies)

import requests
r = requests.get('http://www.cmoney.tw/finance/f00042.aspx?s=' + co_id, cookies)
r.encoding = encoding
#r.status_code
response_html = r.content.decode("utf-8")
'''
Html_file= open("response.html","w")
Html_file.write(response_html)
Html_file.close()
'''

headers = {'GET': '/finance/ashx/mainpage.ashx?action=GetStockListLatestSaleData&stockId=2330&cmkey=CF6a1IO8dx6Caduocze85A%3D%3D&_=1561403345800 HTTP/1.1'
,'Host': 'www.cmoney.tw'
,'Connection': 'keep-alive'
,'Accept': 'application/json, text/javascript, */*; q=0.01'
,'X-Requested-With': 'XMLHttpRequest'
,'User-Agent': 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
,'Referer': 'http://www.cmoney.tw/finance/f00042.aspx?s=' + co_id
,'Accept-Encoding': 'gzip, deflate'
,'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7'
,'Cookie': '_ga=GA1.2.1567679139.1525861239; __gads=ID=be7760519b14e770:T=1525861238:S=ALNI_MYSebuXGIFWVB7SdznkD1Cev3tLlA; __auc=9d320dad16b75724ae281d02957; _fbp=fb.1.1561043554425.739071605; AspSession=lugpcwv0co24acghvhj3wden; ASP.NET_SessionId=knfvra2gizicwrk3rnyysf1h; _gid=GA1.2.370897868.1561398716; _gat_UA-30929682-4=1; _gat_real=1'
}


from lxml import html
tree = html.fromstring(response_html)
a = tree.cssselect('.primary-navi-now a')
cmkey = a[0].attrib['cmkey']
#print(cmkey)
#cmkey = 'CF6a1IO8dx6Caduocze85A%3D%3D'
getstring = 'http://www.cmoney.tw/finance/ashx/mainpage.ashx?action=GetCashFlowStatement&stockId=' + co_id + '&selectedType=3&cmkey=' + cmkey
#print(getstring)
r2 = requests.get(getstring, headers = headers)
r2.encoding = encoding
#print(r2.status_code)
response_json = r2.content.decode("utf-8")

if to_save_json:
	Html_file= open(co_id + "_CashFlowStatement.json","w")
	Html_file.write(response_json)
	Html_file.close()
	#print('save true')
#else:
	#print(response_json.replace('\n', ''))

#print(sys.argv[2])


print(co_id + u' 自由現金流量：')
import json
j = json.loads(response_json.replace('\n', ''))
for r in j:	
	cf = round(int(r['FreeCashFlow'])/1000,2)
	print(r['DateRange'] + ': ' + str(cf))
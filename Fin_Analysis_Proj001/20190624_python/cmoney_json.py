# -*- coding: utf-8 -*
#   python /home/hcc/bitbucket/hccga7_linux/Fin_Analysis_Proj001/20190624_python/cmoney_json.py '2701_CashFlowStatement.json'
import sys
json_file_name = sys.argv[1]
with open(json_file_name, 'r') as file:
    data = file.read().replace('\n', '')
import json
j = json.loads(data)
for r in j:	
	cf = round(int(r['FreeCashFlow'])/1000,2)
	print(r['DateRange'] + ': ' + str(cf))
	#print(r['DateRange'] + ':' + r['FreeCashFlow'])
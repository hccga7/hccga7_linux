# -*- coding: utf-8 -*

#   python /home/hcc/bitbucket/hccga7_linux/Fin_Analysis_Proj001/_Fin_Final_python/Pandas_t163sb08_v3.py '8081'
import glob
import numpy as np
import sys
import sqlite3
import pandas as pd
from pandas.io import sql

file_path_in = '/home/hcc/bitbucket/hccga7_linux/Fin_Analysis_Proj001/_Fin_Index01_csv/'
file_path_out = '/home/hcc/tmp/t163sb08_csv/'

def import_csv_to_sqlite3(co_id):
    file_name = file_path_in + co_id + '.csv'
    df = pd.read_csv(file_name, header=None)
    df.columns = ['year','qtr',u'累計營收',u'累計毛利率',u'累計營業利益率',u'累計稅前純益率',u'累計稅後純益率']
    X = pd.DataFrame(df).dropna()
    X[u'累計毛利'] = X[u'累計營收'] * X[u'累計毛利率'] / 100
    X[u'累計營業利益'] = X[u'累計營收'] * X[u'累計營業利益率'] / 100
    X[u'累計稅前純益'] = X[u'累計營收'] * X[u'累計稅前純益率'] / 100
    X[u'累計稅後純益'] = X[u'累計營收'] * X[u'累計稅後純益率'] / 100
    X[u'本季營收'] = np.nan
    X[u'本季毛利'] = np.nan
    X[u'本季營業利益'] = np.nan
    X[u'本季稅前純益'] = np.nan
    X[u'本季稅後純益'] = np.nan
    X[u'本季毛利率'] = np.nan
    X[u'本季營業利益率'] = np.nan
    X[u'本季稅前純益率'] = np.nan
    X[u'本季稅後純益率'] = np.nan
    for index, row in X.iterrows():
        row[u'本季營收'] = row['累計營收']
        row[u'本季毛利'] = row['累計毛利']
        row[u'本季營業利益'] = row['累計營業利益']
        row[u'本季稅前純益'] = row['累計稅前純益']
        row[u'本季稅後純益'] = row['累計稅後純益']
        row[u'本季毛利率'] = row['累計毛利率']
        row[u'本季營業利益率'] = row['累計營業利益率']
        row[u'本季稅前純益率'] = row['累計稅前純益率']
        row[u'本季稅後純益率'] = row['累計稅後純益率']
        if (row['qtr'] > 1):
            pre_row = X[ (X['year'] == row['year']) & (X['qtr'] == row['qtr']-1) ]
            if len(pre_row) == 1:
                pre_qtr_row = pre_row.iloc[0]
                row[u'本季營收'] = row['累計營收'] - pre_qtr_row['累計營收']
                row[u'本季毛利'] = row['累計毛利'] - pre_qtr_row['累計毛利']
                row[u'本季營業利益'] = row['累計營業利益'] - pre_qtr_row['累計營業利益']
                row[u'本季稅前純益'] = row['累計稅前純益'] - pre_qtr_row['累計稅前純益']
                row[u'本季稅後純益'] = row['累計稅後純益'] - pre_qtr_row['累計稅後純益']
                row[u'本季毛利率'] = 100 * row['本季毛利'] / row[u'本季營收']
                row[u'本季營業利益率'] = 100 * row['本季營業利益'] / row[u'本季營收']
                row[u'本季稅前純益率'] = 100 * row['本季稅前純益'] / row[u'本季營收']
                row[u'本季稅後純益率'] = 100 * row['本季稅後純益'] / row[u'本季營收']
    X.insert(loc=0, column='co_id', value=co_id)
    conn = sqlite3.connect('/home/hcc/tmp/raw_bd6.sqlite3')
    sql.to_sql(X,
                    con=conn,
                    name='tRaw_bd_61',
                    index=False,
                    index_label='co_id',
                    if_exists='append')

for f in glob.glob(file_path_in + "*.csv"):
    f_co_id =  str(f).replace(file_path_in, '').replace('.csv','')
    try:
        import_csv_to_sqlite3(f_co_id)
    except:
        pass

# -*- coding: utf-8 -*
'''
個別綜合損益表
http://mops.twse.com.tw/mops/web/t164sb04
'''
#   python /home/hcc/bitbucket/hccga7_linux/Fin_Analysis_Proj001/_Fin_Final_python/testParse.py '2701'


url = 'http://mops.twse.com.tw/mops/web/t164sb04'
encoding = 'utf8'

'''
encodeURIComponent: 1
step: 1
firstin: 1
off: 1
keyword4: 
code1: 
TYPEK2: 
checkbtn: 
queryName: co_id
inpuType: co_id
TYPEK: all
isnew: false
co_id: 2330
year: 108
season: 01
'''

def download(co_id,year,season):
	import requests
	year_tw = str(year-1911)
	season_str = '0' + str(season)
	payload = {
	  'encodeURIComponent': '1', 'step': '1', 'firstin': '1', 'off': '1', 'keyword4': '', 'code1': '', 'TYPEK2': '', 'checkbtn': '', 'queryName': 'co_id', 'inpuType': 'co_id'
	  , 'TYPEK': 'all', 'isnew': 'false'
	  , 'co_id': co_id
	  , 'year': year_tw
	  , 'season': season_str
	}
	try:
		r = requests.post(url, data = payload)
		r.encoding = encoding
		if r.status_code == 200 :
			response_html = r.content
		else :
			response_html = ''
			print('------------------------------download status_code is not 200')
	except Exception,e:
		print str(e)
		response_html = ''
	
	Html_file= open("response.html","w")
	Html_file.write(response_html)
	Html_file.close()
	
	return response_html

def parse(co_id,year,season):
	response_html = download(co_id,year,season)
	from lxml import html
	tree = html.fromstring(response_html)
	trs = tree.cssselect('table.hasBorder tr')
	idx_tr = 0
	for tr in trs:
		idx_tr = idx_tr + 1
		tds = tr.getchildren()
		if idx_tr == 1:
			print(tds[0].text.strip())
		if tds[0].text.strip() == u'本期綜合損益總額':
			print(tds[0].text.strip() + ': ' + tds[1].text.strip() + u'\t去年度: ' + tds[3].text.strip())
			print(getChangeRate(tds[1].text, tds[3].text))
		if tds[0].text.strip() == u'基本每股盈餘' and not tds[1].text is None:
			print(tds[0].text.strip() + ': ' + tds[1].text.strip())
			#print(str(tds[1].attrib['class']))
			#print(tds[1].attrib['class'])

def getChangeRate(this_year_total,last_year_total):
	from decimal import Decimal
	result_ChangeRate = ''
	try:
		this_year_total = this_year_total.strip().replace(",", "")
		last_year_total = last_year_total.strip().replace(",", "")
		decimal_this_year_total = Decimal(this_year_total)
		decimal_last_year_total = Decimal(last_year_total)
		change_percentage = (decimal_this_year_total - decimal_last_year_total)/decimal_last_year_total * 100
		change_percentage = round(change_percentage,2)
		result_ChangeRate = str(change_percentage) + '%'
	except Exception,e:
		print '---------[getChangeRate() error]: ' + str(e)
		result_ChangeRate = ''
	return result_ChangeRate

import sys
co_id = sys.argv[1]
print(u'個別綜合損益表 摘要：\n')
parse(co_id,2019,1)
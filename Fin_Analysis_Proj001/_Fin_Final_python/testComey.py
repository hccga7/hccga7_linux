# -*- coding: utf-8 -*
#    python /home/hcc/bitbucket/hccga7_linux/Fin_Analysis_Proj001/_Fin_Final_python/testComey.py

'''
營收盈餘
href="/finance/f00029.aspx?s=2330" 
//a[contains(@href, "f00029.aspx")]//@cmkey

損益表
href="/finance/f00041.aspx?s=2330"
//a[contains(@href, "f00041.aspx")]//@cmkey

現金流量表
href="/finance/f00042.aspx?s=2330"
//a[contains(@href, "f00042.aspx")]//@cmkey

財務比率
href="/finance/f00043.aspx?s=2330"
//a[contains(@href, "f00043.aspx")]//@cmkey
'''

import requests
import urllib
from lxml import etree
import urllib.parse

co_id='8499'
encoding = 'utf8'

r = requests.get("https://www.cmoney.tw/finance/f00025.aspx?s="+co_id)
response_html = r.content.decode("utf-8")
html = etree.HTML(response_html)
result = etree.tostring(html)   #Python爬虫利器三之Xpath语法与lxml库的用法   https://cuiqingcai.com/2621.html
#file = open(co_id + '_f00025.html', 'w')
#file.write(response_html)
#file.close()
aspx29 = "f00029.aspx"
aspx41 = "f00041.aspx"
aspx42 = "f00042.aspx"
aspx43 = "f00043.aspx"
cmkey29 = urllib.parse.quote(html.xpath('//a[contains(@href, "/finance/' + aspx29 + '")]//@cmkey')[0]).replace('/','%2F')
cmkey41 = urllib.parse.quote(html.xpath('//a[contains(@href, "/finance/' + aspx41 + '")]//@cmkey')[0]).replace('/','%2F')
cmkey42 = urllib.parse.quote(html.xpath('//a[contains(@href, "/finance/' + aspx42 + '")]//@cmkey')[0]).replace('/','%2F')
cmkey43 = urllib.parse.quote(html.xpath('//a[contains(@href, "/finance/' + aspx43 + '")]//@cmkey')[0]).replace('/','%2F')
action29 = 'GetStockRevenueSurplus'
action41 = 'GetIncomeStatement'
action42 = 'GetCashFlowStatement'
action43 = 'GetFinancialRatios'
selectedType29 = '3'
selectedType41 = '5'
selectedType42 = '3'
selectedType43 = '4'
#---------------------------------------------------------------------------------------
aspx = aspx43
cmkey = cmkey43
action = action43
selectedType = selectedType43
#---------------------------------------------------------------------------------------

headers = {'authority': 'www.cmoney.tw'
,'method': 'GET'
,'path': '/finance/ashx/mainpage.ashx?action=' + action + '&stockId=' + co_id + '&selectedType=' + selectedType + '&cmkey=' + cmkey
,'scheme': 'https'
,'accept': 'application/json, text/javascript, */*; q=0.01'
,'accept-encoding': 'gzip, deflate, br'
,'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7'
,'cookie': '__auc=5d03015516b6e25fdea7ec8f15e; _ga=GA1.2.2060009967.1560921114; __gads=ID=fd016eaa4caf64a4:T=1560921114:S=ALNI_MaSnDYZNjAduiZLQ42JK9F0MQqo4g; _fbp=fb.1.1560921116086.828219937; _gid=GA1.2.838076709.1562244263; __asc=a4aae9a116bbd88b52966ace14c; AspSession=1uupzvgj1qmp0y35434etpso; ASP.NET_SessionId=4ptm14x02waiutfp14qk3bxz; _gat_UA-30929682-4=1; _gat_UA-30929682-1=1; _gat_UA-30929682-32=1; _gat_real=1'
,'referer': 'https://www.cmoney.tw/finance/' + aspx + '?s=' + co_id
,'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
,'x-requested-with': 'XMLHttpRequest'
}
getstring = 'http://www.cmoney.tw/finance/ashx/mainpage.ashx?action=' + action + '&stockId=' + co_id + '&selectedType=' + selectedType + '&cmkey=' + cmkey
response_json = requests.get(getstring, headers = headers)
response_json.encoding = encoding
response_json = response_json.content.decode("utf-8")
file = open(co_id + aspx + '.json', 'w')
file.write(response_json)
file.close()
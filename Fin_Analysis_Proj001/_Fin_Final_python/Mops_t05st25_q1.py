# -*- coding: utf-8 -*

#   python /home/hcc/bitbucket/hccga7_linux/Fin_Analysis_Proj001/_Fin_Final_python/Mops_t05st25_q1.py '5266'

Inventory_Turnover = ''
processing_year = 2013
response_html = ''

url = 'http://mops.twse.com.tw/mops/web/ajax_t05st25'
encoding = 'utf8'

'''
encodeURIComponent: 1
run: Y
step: 1
TYPEK: sii
year: 102
firstin: 1
off: 1
co_id: 2330
isnew: false
ifrs: Y
'''

def download(co_id,year):
	import requests
	year_tw = str(year-1911)
	payload = {
	  'encodeURIComponent': '1', 'run': 'Y', 'step': '1', 'isnew': 'false', 'ifrs': 'Y', 'off': '1', 'firstin': '1'
	  , 'TYPEK': 'all'
	  , 'co_id': co_id
	  , 'year': year_tw
	}
	try:
		r = requests.post(url, data = payload)
		r.encoding = encoding
		if r.status_code == 200 :
			response_html = r.content
			#print('status_code 200')
		else :
			response_html = ''
			print('------------------------------download status_code is not 200')
	except:
		print("Unexpected error:", sys.exc_info()[0])
		response_html = ''

	return response_html

def parse(co_id,year):
	response_html = download(co_id,year)
	from lxml import html
	tree = html.fromstring(response_html)
	trs = tree.cssselect('table.hasBorder tr')
	if len(trs) == 0:
		print(response_html)
		return
	tds1 = trs[1].getchildren()
	tds2 = trs[2].getchildren()
	print(tds1[0].text.strip() + ': ' + tds1[2].text.strip() )
	print(tds2[0].text.strip() + ': ' + tds2[2].text.strip() )

import sys
py_co_id = sys.argv[1]
print(py_co_id + u'存貨周轉率:')
parse(py_co_id,2013)
parse(py_co_id,2015)
parse(py_co_id,2017)
parse(py_co_id,2019)
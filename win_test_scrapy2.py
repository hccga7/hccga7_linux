# -*- coding: utf-8 -*

#   python /home/hcc/bitbucket/hccga7_linux/test_scrapy2.py

#   cd C:\bitbucket\hccga7_linux
#   python C:\bitbucket\hccga7_linux/win_test_scrapy2.py

import time
import os
import os.path
from src.hcc.scrapy.stock import mops as ms
from src.hcc.parse.stock import mops
arr = open('C:\_tmpData\list.txt','r').readlines()
json_folder = 'C:\_tmpData\cmoney_fcf_json' + '\\'
cmkeyURLencoded = ms.get_cmoney_freecf_cmkeyURLencoded('2330')
#cmkeyURLencoded = 'aN3aEkZhWq%2BoRDICFmnyYA%3D%3D'
print(cmkeyURLencoded)
t = 45
for f in arr:
	co_id = f.rstrip('\n')
	my_file = json_folder  + co_id + '_fcf.json'
	if not os.path.isfile(my_file):
		print(co_id + ' begin')
		response_json = ms.download_cmoney_freecf_json(co_id, cmkeyURLencoded)
		m1 = mops.MopsParser()
		m1.save_file(response_json.encode(), json_folder + co_id + '_fcf.json')
		print(co_id + ' save OK')
		print(co_id + ' save sleep ' + str(t) + ' seconds...')
		time.sleep(t)
	else:
		print('skip ' + co_id)
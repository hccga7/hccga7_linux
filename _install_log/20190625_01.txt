﻿# TortoiseGit備忘1
https://blog.csdn.net/dominating_/article/details/81300455
git提交/拉取时换行符不一致导致 提示 "the text is identical, but the files do not match, newlines"等字样
    4.如果使用TortoiseGit，可以在setting的Git配置中选择Global模式，取消选中“AutoCrlf”，然后保存配置

# TortoiseGit備忘2
https://dotblogs.com.tw/kevinya/2017/07/24/180237
執行regedit，並且到以下路徑
HKEY_LOCAL_MACHINE > SOFTWARE > Microsoft > Windows >CurrentVersion >Explorer > ShellIconOverlayIdentifiers



# SmartGit備忘1
SmartGit排除__pycache__資料夾(src模組化後會自動產生此資料夾與.pyc檔)
在.git/info/exclude文件中添加 __pycache__/即可

# SmartGit備忘2
開啟SmartGit會出現"Short read of block"
解決方法：重新從bitbucket抓.git資料夾
例如，從本機Windows10 Git clone後，覆蓋回VM lubuntu中的.git資料夾



# Ubuntu 16.04 安装 PyCharm－Python IDE
http://blog.topspeedsnail.com/archives/6723

sudo add-apt-repository ppa:mystic-mirage/pycharm

sudo apt update
sudo apt install pycharm-community (失敗)
snap install pycharm-community --classic





# region PyCharm code folding

https://www.jetbrains.com/help/objc/code-folding.html#supported_comments

Surrounding a fragment with folding comments #
Select the code fragment of interest.

Select Code | Surround With or press⌥⌘T.

Select the folding comments to be used.

Specify the fragment description.
Now if you collapse the fragment, the description you have specified is shown in place of the code.

https://stackoverflow.com/questions/25135328/how-do-i-make-pycharm-stop-hiding-unfold-my-python-imports/25156495
Open the IDE Settings (File > Settings, or Ctrl+Alt+S).
Under the "Editor" node, click "General" and then "Code Folding". The "Code Folding" page is displayed.
In the "Collapse by default list", select the check boxes to the left of the code constructs you want to be displayed collapsed. So here you can uncheck "Imports".
Apply changes.

# endregion



Sublime Text json formatter
Package Control -> Install Package -> Pretty Json
Ctrl + Alt + j = json 排版



安裝 pymssql
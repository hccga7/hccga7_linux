medium加上簡單區塊的鍵盤指令:
ctrl+alt+6



讓 Windows 與「VirtualBox 裡的 Ubuntu 系統」共用「分享資料夾」
https://briian.com/6241/2/
裝置->共用資料夾->共用資料夾設定->「+」按鈕->資料夾路徑(下拉)->其他->選擇本機資料夾
範例: 資料夾名稱定為 C_DRIVE，lubuntu中再新增/home/hcc/C_DRIVE資料夾
sudo mount -t vboxsf C_DRIVE /home/hcc/C_DRIVE



How to: Linux turn on Num Lock on GNOME startup
https://www.cyberciti.biz/faq/how-to-linux-turn-on-num-lock-on-gnome-startup/
lubuntu開機時自動鎖定numlock
sudo apt-get install numlockx
切換至 /etc/X11/xinit/
以sublime text開啟
# /opt/sublime_text/sublime_text /etc/X11/xinit/xinitrc
/opt/sublime_text/sublime_text .xinitrc



ubuntu-18.04 設置開機啟動腳本[用於分享資料夾、numlock]
https://www.itread01.com/content/1537549089.html

sudo touch /etc/systemd/system/rc-local.service
sudo chmod +x /etc/systemd/system/rc-local.service
sudo systemctl enable rc-local

sudo systemctl start rc-local.service
sudo systemctl status rc-local.service



======================================================================================
以下為試誤版-備忘

Linux 開機自動執行指令
https://blog.gtwang.org/linux/auto-execute-linux-scripts-during-boot-login-logout/
hcc@hcc-pc:~$ chmod +x /opt/my_script.sh
chmod: cannot access '/opt/my_script.sh': No such file or directory
hcc@hcc-pc:~$ cd /opt
hcc@hcc-pc:/opt$ touch my_script.sh
touch: cannot touch 'my_script.sh': Permission denied
hcc@hcc-pc:/opt$ sudo touch my_script.sh
[sudo] password for hcc: 
hcc@hcc-pc:/opt$ sudo touch rc.local
hcc@hcc-pc:/opt$ 


sudo 命令直接添加密码到脚本
https://blog.csdn.net/andanlan/article/details/78207135

hcc@hcc-pc:~$ cd /etc
hcc@hcc-pc:/etc$ sudo touch rc.local
[sudo] password for hcc: 
hcc@hcc-pc:/etc$ 




cd /etc/init/
sudo touch 'hcc.conf'
sudo nano /etc/init/hcc.conf

chmod +x the_file_name
exec /opt/my_script.sh
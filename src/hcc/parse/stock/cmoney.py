# -*- coding: utf-8 -*
from src.hcc.scrapy.stock import cmoney as ss

class CmoneyParser:
    def __init__(self):
        self.co_id = ''
        self.allcmkey_response_html = ''

    def reset_allcmkey_response_html(self, co_id):
        self.co_id = co_id
        self.allcmkey_response_html = ss.getAllCmkeyHtml(co_id)

    def getJson(self,co_id, cmkind):
        if self.co_id != co_id:
            self.reset_allcmkey_response_html(co_id)
        return ss.GetJson(co_id, cmkind, self.allcmkey_response_html)
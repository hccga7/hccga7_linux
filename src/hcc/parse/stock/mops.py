# -*- coding: utf-8 -*

from decimal import Decimal
from lxml import html
import json
import numpy as np
from src.hcc.scrapy.stock import mops

class MopsParser:
	def __init__(self):
		self.t164sb04_parsed = False
		self.response_html_t164sb04 = ''
		self.decimal_t164sb04_net_income_this_year_season = Decimal('NaN')
		self.decimal_t164sb04_net_income_last_year_season = Decimal('NaN')
		self.decimal_t164sb04_eps_this_year_season = Decimal('NaN')
		self.cmoney_freecf_parsed = False
		self.response_json_cmoney_freecf = ''
		self.year_cmoney_freecf01 = np.ma.masked
		self.season_cmoney_freecf01 = np.ma.masked
		self.decimal_cmoney_freecf01 = Decimal('NaN')
		self.year_cmoney_freecf02 = np.ma.masked
		self.season_cmoney_freecf02 = np.ma.masked
		self.decimal_cmoney_freecf02 = Decimal('NaN')
		self.year_cmoney_freecf03 = np.ma.masked
		self.season_cmoney_freecf03 = np.ma.masked
		self.decimal_cmoney_freecf03 = Decimal('NaN')
		self.year_cmoney_freecf04 = np.ma.masked
		self.season_cmoney_freecf04 = np.ma.masked
		self.decimal_cmoney_freecf04 = Decimal('NaN')
		self.year_cmoney_freecf05 = np.ma.masked
		self.season_cmoney_freecf05 = np.ma.masked
		self.decimal_cmoney_freecf05 = Decimal('NaN')
		self.year_cmoney_freecf06 = np.ma.masked
		self.season_cmoney_freecf06 = np.ma.masked
		self.decimal_cmoney_freecf06 = Decimal('NaN')
		self.year_cmoney_freecf07 = np.ma.masked
		self.season_cmoney_freecf07 = np.ma.masked
		self.decimal_cmoney_freecf07 = Decimal('NaN')
		self.year_cmoney_freecf08 = np.ma.masked
		self.season_cmoney_freecf08 = np.ma.masked
		self.decimal_cmoney_freecf08 = Decimal('NaN')

# region [staticmethod]

	@staticmethod
	def save_file(content, filename):
		file = open(filename, 'w')
		file.write(content.decode())
		file.close()

	@staticmethod
	def get_string_from_file(filename):
		f = open(filename, 'r')
		file_content = f.read()
		return file_content

	@staticmethod
	def get_decimal(s):
		return Decimal(s.strip().replace(',', ''))

# endregion

# region [t164sb04 個別綜合損益表]

	def set_response_html_t164sb04_from_file(self, filename):
		self.response_html_t164sb04 = self.get_string_from_file(filename)
		# print(self.response_html_t164sb04)
		# print('set_response_html_t164sb04_from_file OK')

	def init_response_html_t164sb04(self, co_id, year, season):
		if self.response_html_t164sb04 == '':
			self.download_t164sb04(co_id, year, season)

	# 下載 「個別綜合損益表」 html
	def download_t164sb04(self, co_id, year, season):
		self.response_html_t164sb04 = mops.download_t164sb04(co_id, year, season)

	# 儲存 「個別綜合損益表」 html
	def save_file_t164sb04(self, co_id, year, season, filename):
		self.init_response_html_t164sb04(co_id, year, season)
		self.save_file(self.response_html_t164sb04, filename)

	def parse_t164sb04(self, co_id, year, season):
		if self.t164sb04_parsed:
			return

		self.init_response_html_t164sb04(co_id, year, season)
		tree = html.fromstring(self.response_html_t164sb04)
		trs = tree.cssselect('table.hasBorder tr')
		idx_tr = 0
		for tr in trs:
			idx_tr = idx_tr + 1
			tds = tr.getchildren()
			if tds[0].text.strip() == u'本期綜合損益總額':
				self.decimal_t164sb04_net_income_this_year_season = self.get_decimal(tds[1].text)
				self.decimal_t164sb04_net_income_last_year_season = self.get_decimal(tds[3].text)
			if tds[0].text.strip() == u'基本每股盈餘' and not tds[1].text is None:
				self.decimal_t164sb04_eps_this_year_season = self.get_decimal(tds[1].text)

		self.t164sb04_parsed = True

	# 本期綜合損益總額(個別綜合損益表)
	def get_t164sb04_net_income_this_year_season(self, co_id, year, season):
		self.parse_t164sb04(co_id, year, season)
		return self.decimal_t164sb04_net_income_this_year_season

	# 本期綜合損益總額-去年度(個別綜合損益表)
	def get_t164sb04_net_income_last_year_season(self, co_id, year, season):
		self.parse_t164sb04(co_id, year, season)
		return self.decimal_t164sb04_net_income_last_year_season

	# 基本每股盈餘(個別綜合損益表)
	def get_t164sb04_eps_this_year_season(self, co_id, year, season):
		self.parse_t164sb04(co_id, year, season)
		return self.decimal_t164sb04_eps_this_year_season

# endregion

# region [cmoney_freecf 自由現金流量]
	def set_response_json_cmoney_freecf_from_file(self, filename):
		self.response_json_cmoney_freecf = self.get_string_from_file(filename)

	def init_response_json_cmoney_freecf(self, co_id):
		if self.response_json_cmoney_freecf == '':
			self.download_cmoney_freecf(co_id)

	def download_cmoney_freecf(self, co_id):
		self.response_json_cmoney_freecf = mops.download_cmoney_freecf_json(co_id)

	def save_file_cmoney_freecf(self, co_id, filename):
		self.init_response_json_cmoney_freecf(co_id)
		self.save_file(self.response_json_cmoney_freecf.encode(), filename)

	def parse_cmoney_freecf(self, co_id):
		if self.cmoney_freecf_parsed:
			return
		self.init_response_json_cmoney_freecf(co_id)
		j = json.loads(self.response_json_cmoney_freecf.replace('\n', ''))

		idx = 0
		str_datarange = j[idx]['DateRange']
		self.decimal_cmoney_freecf01 = self.get_decimal(j[idx]['FreeCashFlow']) * 1000
		self.year_cmoney_freecf01 = int(str_datarange[0:4])
		self.season_cmoney_freecf01 = int(str_datarange[5:6])

		idx = 1
		str_datarange = j[idx]['DateRange']
		self.decimal_cmoney_freecf02 = self.get_decimal(j[idx]['FreeCashFlow']) * 1000
		self.year_cmoney_freecf02 = int(str_datarange[0:4])
		self.season_cmoney_freecf02 = int(str_datarange[5:6])

		idx = 2
		str_datarange = j[idx]['DateRange']
		self.decimal_cmoney_freecf03 = self.get_decimal(j[idx]['FreeCashFlow']) * 1000
		self.year_cmoney_freecf03 = int(str_datarange[0:4])
		self.season_cmoney_freecf03 = int(str_datarange[5:6])

		idx = 3
		str_datarange = j[idx]['DateRange']
		self.decimal_cmoney_freecf04 = self.get_decimal(j[idx]['FreeCashFlow']) * 1000
		self.year_cmoney_freecf04 = int(str_datarange[0:4])
		self.season_cmoney_freecf04 = int(str_datarange[5:6])

		idx = 4
		str_datarange = j[idx]['DateRange']
		self.decimal_cmoney_freecf05 = self.get_decimal(j[idx]['FreeCashFlow']) * 1000
		self.year_cmoney_freecf05 = int(str_datarange[0:4])
		self.season_cmoney_freecf05 = int(str_datarange[5:6])

		idx = 5
		str_datarange = j[idx]['DateRange']
		self.decimal_cmoney_freecf06 = self.get_decimal(j[idx]['FreeCashFlow']) * 1000
		self.year_cmoney_freecf06 = int(str_datarange[0:4])
		self.season_cmoney_freecf06 = int(str_datarange[5:6])

		idx = 6
		str_datarange = j[idx]['DateRange']
		self.decimal_cmoney_freecf07 = self.get_decimal(j[idx]['FreeCashFlow']) * 1000
		self.year_cmoney_freecf07 = int(str_datarange[0:4])
		self.season_cmoney_freecf07 = int(str_datarange[5:6])

		idx = 7
		str_datarange = j[idx]['DateRange']
		self.decimal_cmoney_freecf08 = self.get_decimal(j[idx]['FreeCashFlow']) * 1000
		self.year_cmoney_freecf08 = int(str_datarange[0:4])
		self.season_cmoney_freecf08 = int(str_datarange[5:6])

		self.cmoney_freecf_parsed = True

# endregion

# -*- coding: utf-8 -*

import requests
import urllib

# 公用下載函式
def download(url, payload, encoding):
    try:
        r = requests.post(url, data=payload)
        r.encoding = encoding
        if r.status_code == 200:
            response_html = r.content
        else:
            response_html = ''
            print('------------------------------download status_code is not 200')
    except Exception as e:
        print(e)
        raise
    return response_html


# 個別綜合損益表
def download_t164sb04(co_id, year, season):
    year_tw = str(year - 1911)
    season_str = '0' + str(season)
    url = 'http://mops.twse.com.tw/mops/web/t164sb04'
    payload = {'encodeURIComponent': '1', 'step': '1', 'firstin': '1', 'off': '1', 'keyword4': '', 'code1': '',
               'TYPEK2': '', 'checkbtn': '', 'queryName': 'co_id', 'inpuType': 'co_id', 'TYPEK': 'all',
               'isnew': 'false',
               'co_id': co_id,
               'year': year_tw,
               'season': season_str
               }
    encoding = 'utf8'
    return download(url, payload, encoding)

def get_cmoney_freecf_cmkeyURLencoded(co_id):
    encoding = 'utf8'
    get_html_url = 'https://www.cmoney.tw/finance/f00042.aspx?s=' + co_id
    headers = {
          'authority': 'www.cmoney.tw'
        , 'method': 'GET'
        , 'path': '/finance/f00042.aspx?s=' + co_id
        , 'scheme': 'https'
        , 'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3'
        , 'accept-encoding': 'gzip, deflate, br'
        , 'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7'
        , 'cookie': '__auc=5d03015516b6e25fdea7ec8f15e; _ga=GA1.2.2060009967.1560921114; __gads=ID=fd016eaa4caf64a4:T=1560921114:S=ALNI_MaSnDYZNjAduiZLQ42JK9F0MQqo4g; _fbp=fb.1.1560921116086.828219937; secondRemind=neverRemine; AspSession=va1vfeiem4dy5xpgdmroexbg; __asc=97b696ed16ba08d92594cda45f0; ASP.NET_SessionId=laiw0a3yuxmjeqlyc0tby1o0; _gid=GA1.2.1168566139.1561766763; _gat_real=1; _gat_UA-30929682-4=1; _gat_UA-30929682-1=1; _gat_UA-30929682-32=1'
        , 'upgrade-insecure-requests': '1'
    }
    r = requests.get(get_html_url, headers=headers)
    r.encoding = encoding
    response_html = r.content.decode("utf-8")
    #print(response_html)
    from lxml import html
    tree = html.fromstring(response_html)
    a = tree.cssselect('.primary-navi-now a')
    cmkey = a[0].attrib['cmkey']
    #print('get cmkey from cmoney: ' + cmkey)
    cmkeyURLencoded = urllib.parse.quote(cmkey)
    #print(cmkeyURLencoded)
    return cmkeyURLencoded

def download_cmoney_freecf_json(co_id, cmkeyURLencoded=None):
    encoding = 'utf8'
    if cmkeyURLencoded==None:
        cmkeyURLencoded = get_cmoney_freecf_cmkeyURLencoded(co_id)
        print('cmkeyURLencoded==None')
    get_json_url = 'https://www.cmoney.tw/finance/ashx/mainpage.ashx?action=GetCashFlowStatement&stockId=' + co_id + '&selectedType=3&cmkey=' + cmkeyURLencoded
    headers = {
          'authority': 'www.cmoney.tw'
        , 'method': 'GET'
        , 'path': '/finance/ashx/mainpage.ashx?action=GetCashFlowStatement&stockId=' + co_id + '&selectedType=3&cmkey=' + cmkeyURLencoded
        , 'scheme': 'https'
        , 'accept': 'application/json, text/javascript, */*; q=0.01'
        , 'accept-encoding': 'gzip, deflate, br'
        , 'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7'
        , 'cookie': '__auc=5d03015516b6e25fdea7ec8f15e; _ga=GA1.2.2060009967.1560921114; __gads=ID=fd016eaa4caf64a4:T=1560921114:S=ALNI_MaSnDYZNjAduiZLQ42JK9F0MQqo4g; _fbp=fb.1.1560921116086.828219937; secondRemind=neverRemine; AspSession=va1vfeiem4dy5xpgdmroexbg; __asc=97b696ed16ba08d92594cda45f0; ASP.NET_SessionId=laiw0a3yuxmjeqlyc0tby1o0; _gid=GA1.2.1168566139.1561766763; _gat_real=1; _gat_UA-30929682-4=1; _gat_UA-30929682-1=1; _gat_UA-30929682-32=1'
        , 'referer': 'https://www.cmoney.tw/finance/f00042.aspx?s=' + co_id
        , 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
        , 'x-requested-with': 'XMLHttpRequest'
    }
    r_json = requests.get(get_json_url, headers=headers)
    r_json.encoding = encoding
    response_json = r_json.content.decode("utf-8")
    return response_json

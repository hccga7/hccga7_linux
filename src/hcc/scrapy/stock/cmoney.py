# -*- coding: utf-8 -*
import requests
from lxml import etree
import urllib.parse

def GetAspxFromCmkind(cmkind):
    aspx = "f000" + cmkind + ".aspx"   # f00029.aspx, f00041.aspx, f00042.aspx, ...
    return aspx

def GetActionFromCmkind(cmkind):
    action = ''
    if(cmkind=='29'):
        action = 'GetStockRevenueSurplus'
    elif(cmkind=='41'):
        action = 'GetIncomeStatement'
    elif(cmkind=='42'):
        action = 'GetCashFlowStatement'
    elif(cmkind=='43'):
        action = 'GetFinancialRatios'
    else:
        action = ''    
    return action

def GetSelectedTypeFromCmkind(cmkind):
    selectedType = ''
    if(cmkind=='29'):
        selectedType = '3'
    elif(cmkind=='41'):
        selectedType = '5'
    elif(cmkind=='42'):
        selectedType = '3'
    elif(cmkind=='43'):
        selectedType = '4'
    else:
        selectedType = ''    
    return selectedType

def GetCmkeyFromHtml(response_html, cmkind):
    html = etree.HTML(response_html)
    aspx = GetAspxFromCmkind(cmkind)
    cmkey = urllib.parse.quote(html.xpath('//a[contains(@href, "/finance/' + aspx + '")]//@cmkey')[0]).replace('/','%2F')
    return cmkey

def getAllCmkeyHtml(co_id):
    url = 'https://www.cmoney.tw/finance/f00025.aspx?s=' + co_id
    encoding = 'utf8'
    r = requests.get(url)
    response_html = r.content.decode(encoding)
    return response_html

def GetJson(co_id, cmkind, response_html):
    encoding = 'utf8'
    cmkey = GetCmkeyFromHtml(response_html, cmkind)
    aspx = GetAspxFromCmkind(cmkind)
    action = GetActionFromCmkind(cmkind)
    selectedType = GetSelectedTypeFromCmkind(cmkind)
    headers = {'authority': 'www.cmoney.tw'
    ,'method': 'GET'
    ,'path': '/finance/ashx/mainpage.ashx?action=' + action + '&stockId=' + co_id + '&selectedType=' + selectedType + '&cmkey=' + cmkey
    ,'scheme': 'https'
    ,'accept': 'application/json, text/javascript, */*; q=0.01'
    ,'accept-encoding': 'gzip, deflate, br'
    ,'accept-language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7'
    ,'cookie': '__auc=5d03015516b6e25fdea7ec8f15e; _ga=GA1.2.2060009967.1560921114; __gads=ID=fd016eaa4caf64a4:T=1560921114:S=ALNI_MaSnDYZNjAduiZLQ42JK9F0MQqo4g; _fbp=fb.1.1560921116086.828219937; _gid=GA1.2.838076709.1562244263; __asc=a4aae9a116bbd88b52966ace14c; AspSession=1uupzvgj1qmp0y35434etpso; ASP.NET_SessionId=4ptm14x02waiutfp14qk3bxz; _gat_UA-30929682-4=1; _gat_UA-30929682-1=1; _gat_UA-30929682-32=1; _gat_real=1'
    ,'referer': 'https://www.cmoney.tw/finance/' + aspx + '?s=' + co_id
    ,'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
    ,'x-requested-with': 'XMLHttpRequest'
    }
    getstring = 'http://www.cmoney.tw/finance/ashx/mainpage.ashx?action=' + action + '&stockId=' + co_id + '&selectedType=' + selectedType + '&cmkey=' + cmkey
    response_json = requests.get(getstring, headers = headers)
    response_json.encoding = encoding
    #response_json = response_json.content.decode("utf-8")
    response_json = response_json.content.decode(encoding)
    return response_json